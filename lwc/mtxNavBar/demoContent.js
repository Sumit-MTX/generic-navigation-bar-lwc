const items = [
    { id: 1, name: 'Item 1', icon: 'utility:home', identifier: 'item1' },
    { id: 2, name: 'Item 2', icon: 'utility:home', identifier: 'item2' },
    { id: 3, name: 'Item 3', icon: 'utility:home', identifier: 'item3' },
    { id: 4, name: 'Item 4', icon: 'utility:home', identifier: 'item4' },
    { id: 5, name: 'Item 5', icon: 'utility:home', identifier: 'item5' },
];

export { items };