import { LightningElement, api, track } from 'lwc';
import { items } from './demoContent';

export default class MtxNavBar extends LightningElement {
    @api items;
    @track testData = items; // testdata

    handleItemSelection(event) {
        const activeItem = this.template.querySelector('.active');

        // removing the highlight from the current item and placing it on the item which was clicked
        if(activeItem) {
            activeItem.classList.toggle('active');
        }
        event.currentTarget.classList.add('active');

        // dispatching an event with the name from the dataset attributes as the unique identifier, can be changed to anything as per dev
        this.dispatchEvent(new CustomEvent('itemselection', { detail: { name: event.currentTarget.dataset.name } }));
    }
}