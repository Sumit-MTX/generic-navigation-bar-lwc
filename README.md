## Description of the Asset :
- This asset is a navigation bar which is generic enough to be used in any project and highly customizable to incorporate any changes very easily.
- Developed for use in LWC only.
- This has been used in multiple Internal and External projects such as MTX Estimator, MTX STEP Training Portal, etc.
- This component is styled in a blue theme, It can be modified to adjust to any other color theme easily.

## Purpose of the Asset :
- This asset is a direct implementation of a navigation bar which is often required in many lwc projects.

## Usage of the Asset :
- The component takes in 1 parameter : `items`
- `items` is an array of objects which define multiple parameters such as the name of the items, their uniqueness and the type of slds-icon that should be used for them.
- A sample of how `items` look is present in the `demoContent.js` file present in the component directory itself.
- After selecting an item, it emits an event named `itemselection` where you will get the name of the item from the dataset attributes of that item.
- It can be easily modified to incorporate the toggle button which shrinks the nav bar. Will update the nav bar code with that change soon.

## To deploy the component :
- Navigate inside the folder with the package.xml file
- Run `sfdx:force:source:deploy -x package.xml`
